// ============== Data =========================
const monstersData = [{
        'fname': 'Anbu',
        'lname': 'Arasan',
        'phone': '190-309-6101',
        'email': 'anbu.arasan@email.com'
    },
    {
        'fname': 'Arivu',
        'lname': 'Mugilan',
        'phone': '490-701-7102',
        'email': 'arivu.mugilan@email.com'
    },
    {
        'fname': 'Bob',
        'lname': 'Johnson',
        'phone': '574-909-3948',
        'email': 'bob.johnson@email.com'
    },
    {
        'fname': 'Raja',
        'lname': 'Tamil',
        'phone': '090-909-0101',
        'email': 'raja.tamil@email.com'
    },
    {
        'fname': 'Sundar',
        'lname': 'Kannan',
        'phone': '090-909-0101',
        'email': 'sundar.kannan@email.com'
    }
]





// ============== Controller (API) =========================

class MonsterBookCtrl {

    constructor(monsterBookView) {
        this.monsterBookView = monsterBookView;
    }

    init() {
        this.monsterBookView.init();
    }

    getMonsters() {
        return monstersData;
    }
    getMonster(index) {
        return monstersData[index];
    }
    addMonster(monster) {
        monstersData.push(monster);
    }

}



// ============== VIEW =========================
class MonsterBookView {

    init() {
        this.renderMonsterListModule();
        this.renderMonsterDetailsModule(0);
        this.addMonsterModule();
    }

    //--------
    // ADD
    //--------
    addMonsterModule() {
        const $addMonster = document.getElementById('add-monster-btn');
        $addMonster.addEventListener("click", this.addMonsterBtnClicked.bind(this));
    }
    addMonsterBtnClicked() {

        // get the add contact form inputs 
        const $addMonsterInputs = document.getElementsByClassName('add-monster-input');

        // this object will hold the new contact information
        let newMonster = {};

        // loop through View to get the data for the model 
        for (let i = 0, len = $addMonsterInputs.length; i < len; i++) {

            let key = $addMonsterInputs[i].getAttribute('data-key');
            let value = $addMonsterInputs[i].value;
            newMonster[key] = value;
        }
        // passing new object to the addContact method 
        monsterBookApp.addMonster(newMonster);

        // render the contact list with the new data set
        this.renderMonsterListModule();
    }

    renderMonsterDetailsModule(e) {
        let selectedIndex = null;

        if (typeof e === 'object') {
            e.stopPropagation();
            selectedIndex = e.currentTarget.getAttribute('data-index');
        } else {
            selectedIndex = e;
        }
        const selectedItem = monsterBookApp.getMonster(selectedIndex);

        const $MonsterItemUI = document.getElementById('monster-item-details');

        $MonsterItemUI.innerHTML = `${selectedItem['fname']} <br> ${selectedItem['lname']} <br> ${selectedItem['phone']} <br> ${selectedItem['email']}`;

        this.hightlightCurrentListItem(selectedIndex);
    }

    hightlightCurrentListItem(selectedIndex) {
        const $MonsterListItems = document.getElementsByClassName(`monster-list-item`);
        for (let i = 0, len = $MonsterListItems.length; i < len; i++) {
            //Wat doet dit active?
            $MonsterListItems[i].classList.remove('active');
        }
        $MonsterListItems[selectedIndex].classList.add("active");
    }


    // -----------------------------------
    // RENDER 
    // -----------------------------------
    renderMonsterListModule() {

        // model
        const monsters = monsterBookApp.getMonsters();

        // view 
        const $MonsterListUI = document.getElementById('monster-list')
        $MonsterListUI.innerHTML = '';


        // ctrl
        for (let i = 0, len = monsters.length; i < len; i++) {

            // list item
            let $li = document.createElement('li');
            $li.setAttribute('class', 'monster-list-item');
            $li.setAttribute('data-index', i);

            // label div
            let $div = document.createElement('div');
            $div.innerHTML = `${monsters[i]['fname']}, <strong>${monsters[i]['lname']}</strong> `;

            $li.append($div);
            $li.addEventListener("click", this.renderMonsterDetailsModule.bind(this));
            $MonsterListUI.append($li);
        }

    }

}

// create an object from the class MonsterBookView
const monsterBookView = new MonsterBookView();

// create an object from MonsterBookCtrl and passing MonsterBookView in the constructor as a dependent
const monsterBookApp = new MonsterBookCtrl(monsterBookView);

// App starting...
monsterBookApp.init();